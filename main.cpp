/*
 * main.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: debian
 */

#include <iostream>

#include "include/vector.hpp"

template<class T, std::size_t dim>
void print_vector(math::vectors::Vector<T, dim> in)
{
	using std::cout;
	cout << "(";
	for(unsigned int i = 0; i < dim; ++i)
	{
		cout << in[i];
		if(i < dim-1) cout << ", ";
	}
	cout << ")\n";
}

int main(void)
{
	using std::cout;
	using namespace math::vectors;
	const vec3f test1
	{ 1.0, 0.5, 0.25 };
	vec3f test2
	{ 2.0, 0.25, 0.87 };

	vec3f test3;

	print_vector(test3);
	test3.x = 0.1;
	test3.y = 0.2;
	test3.z = -0.1;
	print_vector(test3);

	vec2 test4;

	test4.x = 5.0;
	test4.y = 5.0;

	//test3.x() = 0.0;
	float dotp;

	if(test1 == test2)
	{
		print_vector(test1);
		cout << " == \n";
		print_vector(test2);
	}

	if(test1 != test2)
	{
		print_vector(test1);
		cout << " != \n";
		print_vector(test2);
	}

	if(test1 <= test2)
	{
		print_vector(test1);
		cout << " <= \n";
		print_vector(test2);
	}

	if(test1 >= test2)
	{
		print_vector(test1);
		cout << " >= \n";
		print_vector(test2);
	}

	if(test1 < test2)
	{
		print_vector(test1);
		cout << " < \n";
		print_vector(test2);
	}

	if(test1 > test2)
	{
		print_vector(test1);
		cout << " > \n";
		print_vector(test2);
	}

	test3 = test1 + test2;
	print_vector(test3);

	test3 = test1 - test2;
	print_vector(test3);

	test3 = test1 * test2;
	print_vector(test3);

	test3 = test1 / test2;
	print_vector(test3);

	test3 = test1;
	test3 += test2;
	print_vector(test3);

	test3 = test1;
	test3 -= test2;
	print_vector(test3);

	test3 = test1;
	test3 *= test2;
	print_vector(test3);

	test3 = test1;
	test3 /= test2;
	print_vector(test3);

	test3 = test1 + 1.;
	print_vector(test3);

	test3 = test1 + 1;
	print_vector(test3);

	test3 = test1 - 1.f;
	print_vector(test3);

	test3 = test1 - 1.;
	print_vector(test3);

	test3 = test1 - 1;
	print_vector(test3);

	test3 = test1 * 2.f;
	print_vector(test3);

	test3 = test1 * 2.;
	print_vector(test3);

	test3 = test1 * 2;
	print_vector(test3);

	test3 = test1 / 2.f;
	print_vector(test3);

	test3 = test1 / 2.;
	print_vector(test3);

	test3 = test1 / 2;
	print_vector(test3);

	test3 = 1.f + test1;
	print_vector(test3);

	test3 = 1. + test1;
	print_vector(test3);

	test3 = 1 + test1;
	print_vector(test3);

	test3 = 1.f - test1;
	print_vector(test3);

	test3 = 1. - test1;
	print_vector(test3);

	test3 = 1 - test1;
	print_vector(test3);

	test3 = 2.f * test1;
	print_vector(test3);

	test3 = 2. * test1;
	print_vector(test3);

	test3 = 2 * test1;
	print_vector(test3);

	test3 = 1.f / test1;
	print_vector(test3);

	test3 = 1. / test1;
	print_vector(test3);

	test3 = 1 / test1;
	print_vector(test3);

	dotp = test1.dot(test1);
	cout << dotp << "\n";

	test3 = test1.cross(test2);
	print_vector(test3);

	dotp = test1.length();
	cout << dotp << "\n";

	test3 = test1.normalize();
	print_vector(test3);

	dotp = test1.length();
	cout << dotp << "\n";

	dotp = test3.length();
	cout << dotp << "\n";

	test3 = test2.normalize();
	print_vector(test3);

	dotp = test2.length();
	cout << dotp << "\n";

	dotp = test3.length();
	cout << dotp << "\n";

	//cout << test3;

	return 0;
}

