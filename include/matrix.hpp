/*
 * matrix.hpp
 *
 *  Created on: Jul 20, 2016
 *      Author: debian
 */

#ifndef MATRIX_HPP_
#define MATRIX_HPP_

#include "vector.hpp"

namespace math
{
namespace matrices
{
template<class T, std::size_t m, std::size_t n>
class Matrix;

template<class T, std::size_t m, std::size_t n>
T & at(Matrix<T, m, n>& lhs, std::size_t idx);

template<class T, std::size_t m, std::size_t n>
T const& at(Matrix<T, m, n> const& lhs, std::size_t idx);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n>& copy(Matrix<T, m, n>& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> uplus(Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> uminus(Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> add(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n>& add(Matrix<T, m, n> & lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> add(Matrix<T, m, n> const& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n>& add(Matrix<T, m, n> & lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> sub(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n>& sub(Matrix<T, m, n> & lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> sub(Matrix<T, m, n> const& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n>& sub(Matrix<T, m, n> & lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> mul(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> & mul(Matrix<T, m, n> & lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> mul(Matrix<T, m, n> const& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n>& mul(Matrix<T, m, n> & lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> div(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n>& div(Matrix<T, m, n> & lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> div(Matrix<T, m, n> const& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n>& div(Matrix<T, m, n> & lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> div(S const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n>& div(S const& lhs, Matrix<T, m, n> & rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> inc(Matrix<T, m, n>& lhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n>& inc(Matrix<T, m, n>& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> dec(Matrix<T, m, n>& lhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n>& dec(Matrix<T, m, n>& rhs);

template<class T, std::size_t m, std::size_t n>
bool eq(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
bool neq(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
bool lt(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
bool le(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
bool gt(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
bool ge(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
T dot(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
T length(Matrix<T, m, n> const& lhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> normalize(Matrix<T, m, n> const& lhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n>& normalize(Matrix<T, m, n>& lhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> cross(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n>& operator++(Matrix<T, m, n>& lhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> operator++(Matrix<T, m, n> const& lhs, int);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n>& operator--(Matrix<T, m, n>& lhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> operator--(Matrix<T, m, n> const& lhs, int);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> operator+(Matrix<T, m, n> const& lhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> operator-(Matrix<T, m, n> const& lhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> operator+(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> operator+(Matrix<T, m, n> const& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> operator+(S const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n>& operator+=(Matrix<T, m, n>& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n>& operator+=(Matrix<T, m, n>& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n> operator-(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> operator-(Matrix<T, m, n> const& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> operator-(S const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
Matrix<T, m, n>& operator-=(Matrix<T, m, n>& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n>& operator-=(Matrix<T, m, n>& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> operator*(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> operator*(Matrix<T, m, n> const& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> operator*(S const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n>& operator*=(Matrix<T, m, n>& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n>& operator*=(Matrix<T, m, n>& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> operator/(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> operator/(Matrix<T, m, n> const& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n> operator/(S const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n>& operator/=(Matrix<T, m, n>& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n, class S>
Matrix<T, m, n>& operator/=(Matrix<T, m, n>& lhs, S const& rhs);

template<class T, std::size_t m, std::size_t n>
bool operator==(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
bool operator!=(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
bool operator<(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
bool operator<=(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
bool operator>(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
bool operator>=(Matrix<T, m, n> const& lhs, Matrix<T, m, n> const& rhs);

template<class T, std::size_t m, std::size_t n>
class Matrix
{
protected:
	vectors::Vector<T, n> rows[m];
public:
	Matrix()
	{

	}


};

}
}

#endif /* MATRIX_HPP_ */
