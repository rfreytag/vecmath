/*
 * vector_sse2.h
 *
 *  Created on: Jan 5, 2017
 *      Author: debian
 */

#ifndef INCLUDE_VECTOR_SSE2_H_
#define INCLUDE_VECTOR_SSE2_H_

#include "vector_base.h"

#include <xmmintrin.h>
#include <emmintrin.h>

namespace math
{
namespace vectors
{

// ===============================================
// Definitions - SSE
// ===============================================

// register load helper

template<class T, unsigned long dim>
__m128 _mm_load_psvn(Vector<T, dim> const& lhs)
{
	static_assert(!(dim < 1 || dim > 4 || !std::is_same<float, T>::value), "_mm_load_psvn not implemented for given template arguments!");
	return 0;
}

template<>
__m128 _mm_load_psvn<float, 2>(Vector<float, 2> const& lhs)
{
	__m128 tmp = _mm_loadl_pi(_mm_setzero_ps(), (const __m64 *) lhs.values);
	return tmp;
}

template<>
__m128 _mm_load_psvn<float, 3>(Vector<float, 3> const& lhs)
{
	__m128i xy = _mm_loadl_epi64((const __m128i *) lhs.values);
	__m128 z = _mm_load_ss(&(lhs.values[2]));
	__m128 tmp = _mm_movelh_ps(_mm_castsi128_ps(xy), z);

	return tmp;
}

template<>
__m128 _mm_load_psvn<float, 4>(Vector<float, 4> const& lhs)
{
	__m128 tmp = _mm_load_ps(lhs.values);
	return tmp;
}

// register store helper

template<class T, unsigned long dim>
inline Vector<T, dim> _mm_store_psvn(__m128  const & lhs)
{
	static_assert(!(dim < 1 || dim > 4 || !std::is_same<float, T>::value), "_mm_store_psvn not implemented for given template arguments!");
	return 0;
}

template<>
inline Vector<float, 1> _mm_store_psvn(__m128  const & lhs)
{
	Vector<float, 1> tmp;
	float values[4] =
	{ };

	_mm_store1_ps(values, lhs);

	memcpy(tmp.values, values, sizeof(float) * 1);

	return tmp;
}

template<>
inline Vector<float, 2> _mm_store_psvn(__m128  const & lhs)
{
	Vector<float, 2> tmp;
	float values[4] =
	{ };

	_mm_store1_ps(values, lhs);

	memcpy(tmp.values, values, sizeof(float) * 2);

	return tmp;
}

template<>
inline Vector<float, 3> _mm_store_psvn(__m128  const & lhs)
{
	Vector<float, 3> tmp;
	float values[4] =
	{ };

	_mm_store1_ps(values, lhs);

	memcpy(tmp.values, values, sizeof(float) * 3);

	return tmp;
}

template<>
inline Vector<float, 4> _mm_store_psvn(__m128  const & lhs)
{
	Vector<float, 4> tmp;

	_mm_store1_ps(tmp.values, lhs);

	return tmp;
}

// add

template<>
Vector<float, 2> add<float, 2>(Vector<float, 2> const& lhs,
		Vector<float, 2> const& rhs)
{
	__m128 op1 = _mm_load_psvn(lhs);
	__m128 op2 = _mm_load_psvn(rhs);
	__m128 res = _mm_add_ps(op1, op2);

	return _mm_store_psvn<float, 2>(res);
}

template<>
Vector<float, 3> add<float, 3>(Vector<float, 3> const& lhs,
		Vector<float, 3> const& rhs)
{
	__m128 op1 = _mm_load_psvn(lhs);
	__m128 op2 = _mm_load_psvn(rhs);
	__m128 res = _mm_add_ps(op1, op2);

	return _mm_store_psvn<float, 3>(res);
}

template<>
Vector<float, 4> add<float, 4>(Vector<float, 4> const& lhs,
		Vector<float, 4> const& rhs)
{
	__m128 op1 = _mm_load_ps(lhs.values);
	__m128 op2 = _mm_load_ps(rhs.values);
	__m128 res = _mm_add_ps(op1, op2);

	return _mm_store_psvn<float, 4>(res);
}

// subtract

template<>
Vector<float, 2> sub<float, 2>(Vector<float, 2> const& lhs,
		Vector<float, 2> const& rhs)
{
	__m128 op1 = _mm_load_psvn(lhs);
	__m128 op2 = _mm_load_psvn(rhs);
	__m128 res = _mm_sub_ps(op1, op2);

	return _mm_store_psvn<float, 2>(res);
}

template<>
Vector<float, 3> sub<float, 3>(Vector<float, 3> const& lhs,
		Vector<float, 3> const& rhs)
{
	__m128 op1 = _mm_load_psvn(lhs);
	__m128 op2 = _mm_load_psvn(rhs);
	__m128 res = _mm_sub_ps(op1, op2);

	return _mm_store_psvn<float, 3>(res);
}

template<>
Vector<float, 4> sub<float, 4>(Vector<float, 4> const& lhs,
		Vector<float, 4> const& rhs)
{
	__m128 op1 = _mm_load_ps(lhs.values);
	__m128 op2 = _mm_load_ps(rhs.values);
	__m128 res = _mm_sub_ps(op1, op2);
	return _mm_store_psvn<float, 4>(res);
}

// multiply

template<>
Vector<float, 2> mul<float, 2>(Vector<float, 2> const& lhs,
		Vector<float, 2> const& rhs)
{
	__m128 op1 = _mm_load_psvn(lhs);
	__m128 op2 = _mm_load_psvn(rhs);
	__m128 res = _mm_mul_ps(op1, op2);

	return _mm_store_psvn<float, 2>(res);
}

template<>
Vector<float, 3> mul<float, 3>(Vector<float, 3> const& lhs,
		Vector<float, 3> const& rhs)
{
	__m128 op1 = _mm_load_psvn(lhs);
	__m128 op2 = _mm_load_psvn(rhs);
	__m128 res = _mm_mul_ps(op1, op2);

	return _mm_store_psvn<float, 3>(res);
}

template<>
Vector<float, 4> mul<float, 4>(Vector<float, 4> const& lhs,
		Vector<float, 4> const& rhs)
{
	__m128 op1 = _mm_load_ps(lhs.values);
	__m128 op2 = _mm_load_ps(rhs.values);
	__m128 res = _mm_mul_ps(op1, op2);
	return _mm_store_psvn<float, 4>(res);
}

// divide

template<>
Vector<float, 2> div<float, 2>(Vector<float, 2> const& lhs,
		Vector<float, 2> const& rhs)
{
	__m128 op1 = _mm_load_psvn(lhs);
	__m128 op2 = _mm_load_psvn(rhs);
	__m128 res = _mm_div_ps(op1, op2);

	return _mm_store_psvn<float, 2>(res);
}

template<>
Vector<float, 3> div<float, 3>(Vector<float, 3> const& lhs,
		Vector<float, 3> const& rhs)
{
	__m128 op1 = _mm_load_psvn(lhs);
	__m128 op2 = _mm_load_psvn(rhs);
	__m128 res = _mm_div_ps(op1, op2);

	return _mm_store_psvn<float, 3>(res);
}

template<>
Vector<float, 4> div<float, 4>(Vector<float, 4> const& lhs,
		Vector<float, 4> const& rhs)
{
	__m128 op1 = _mm_load_ps(lhs.values);
	__m128 op2 = _mm_load_ps(rhs.values);
	__m128 res = _mm_div_ps(op1, op2);
	return _mm_store_psvn<float, 4>(res);
}

// dot

template<>
float dot<float, 4>(Vector<float, 4> const& lhs, Vector<float, 4> const& rhs)
{
	__m128 op1 = _mm_load_ps(lhs.values);
	__m128 op2 = _mm_load_ps(rhs.values);

	__m128 res = _mm_mul_ps(op1, op2);

	op1 = _mm_shuffle_ps(res, res, _MM_SHUFFLE(0, 1, 2, 3));
	op1 = _mm_add_ps(op1, res);
	op2 = _mm_shuffle_ps(op1, op1, _MM_SHUFFLE(2, 3, 0, 1));
	res = _mm_add_ps(op2, op1);

	return _mm_store_psvn<float, 1>(res).x;
}

// cross

template<>
Vector<float, 3> cross(Vector<float, 3> const& lhs, Vector<float, 3> const& rhs)
{
	__m128 op1 = _mm_load_psvn(lhs);
	__m128 op2 = _mm_load_psvn(rhs);

	__m128 t1 = _mm_shuffle_ps(op1, op1, _MM_SHUFFLE(1, 2, 0, 0));
	__m128 t2 = _mm_shuffle_ps(op2, op2, _MM_SHUFFLE(2, 0, 1, 0));
	__m128 m1 = _mm_mul_ps(t1, t2);
	__m128 m2;

	t1 = _mm_shuffle_ps(op1, op1, _MM_SHUFFLE(2, 0, 1, 0));
	t2 = _mm_shuffle_ps(op2, op2, _MM_SHUFFLE(1, 2, 0, 0));
	m2 = _mm_mul_ps(t1, t2);

	t1 = _mm_sub_ps(m1, m2);

	return _mm_store_psvn<float, 3>(t1);
}

template<>
Vector<float, 4> cross(Vector<float, 4> const& lhs, Vector<float, 4> const& rhs)
{
	Vector<float, 4> tmp;

	__m128 op1 = _mm_load_psvn(lhs);
	__m128 op2 = _mm_load_psvn(rhs);

	__m128 t1 = _mm_shuffle_ps(op1, op1, _MM_SHUFFLE(1, 2, 0, 0));
	__m128 t2 = _mm_shuffle_ps(op2, op2, _MM_SHUFFLE(2, 0, 1, 0));
	__m128 m1 = _mm_mul_ps(t1, t2);
	__m128 m2;
	t1 = _mm_shuffle_ps(op1, op1, _MM_SHUFFLE(2, 0, 1, 0));
	t2 = _mm_shuffle_ps(op2, op2, _MM_SHUFFLE(1, 2, 0, 0));
	m2 = _mm_mul_ps(t1, t2);

	t1 = _mm_sub_ps(m1, m2);

	return _mm_store_psvn<float, 4>(t1);
}
}
}


#endif /* INCLUDE_VECTOR_SSE2_H_ */
