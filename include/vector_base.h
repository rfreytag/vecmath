/*
 * vector_base.h
 *
 *  Created on: Jan 5, 2017
 *      Author: debian
 */

#ifndef INCLUDE_VECTOR_BASE_H_
#define INCLUDE_VECTOR_BASE_H_

/*
 * vector.hpp
 *
 *  Created on: Jul 20, 2016
 *      Author: debian
 */


#include <cmath>
#include <cstring>
#include <stdexcept>
#include <initializer_list>
#include <algorithm>

/*
 #include <ios>
 #include <ostream>
 #include <iomanip>
 */

namespace math
{
namespace vectors
{

// ===============================================
// Declarations
// ===============================================

template<class T, std::size_t dim>
class Vector;

/*
 template<class T, unsigned int dim>
 std::ostream& operator<<(std::ostream& os, Vector<T, dim> data);
 */

template<class T, std::size_t dim>
T & at(Vector<T, dim>& lhs, std::size_t idx);

template<class T, std::size_t dim>
T const& at(Vector<T, dim> const& lhs, std::size_t idx);

template<class T, std::size_t dim>
Vector<T, dim>& copy(Vector<T, dim>& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
Vector<T, dim> uplus(Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
Vector<T, dim> uminus(Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
Vector<T, dim> add(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
Vector<T, dim>& add(Vector<T, dim> & lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> add(Vector<T, dim> const& lhs, S const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim>& add(Vector<T, dim> & lhs, S const& rhs);

template<class T, std::size_t dim>
Vector<T, dim> sub(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
Vector<T, dim>& sub(Vector<T, dim> & lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> sub(Vector<T, dim> const& lhs, S const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim>& sub(Vector<T, dim> & lhs, S const& rhs);

template<class T, std::size_t dim>
Vector<T, dim> mul(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
Vector<T, dim> & mul(Vector<T, dim> & lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> mul(Vector<T, dim> const& lhs, S const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim>& mul(Vector<T, dim> & lhs, S const& rhs);

template<class T, std::size_t dim>
Vector<T, dim> div(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
Vector<T, dim>& div(Vector<T, dim> & lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> div(Vector<T, dim> const& lhs, S const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim>& div(Vector<T, dim> & lhs, S const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> div(S const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim>& div(S const& lhs, Vector<T, dim> & rhs);

template<class T, std::size_t dim>
Vector<T, dim> inc(Vector<T, dim>& lhs);

template<class T, std::size_t dim>
Vector<T, dim>& inc(Vector<T, dim>& rhs);

template<class T, std::size_t dim>
Vector<T, dim> dec(Vector<T, dim>& lhs);

template<class T, std::size_t dim>
Vector<T, dim>& dec(Vector<T, dim>& rhs);

template<class T, std::size_t dim>
bool eq(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
bool neq(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
bool lt(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
bool le(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
bool gt(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
bool ge(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
T dot(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
T length(Vector<T, dim> const& lhs);

template<class T, std::size_t dim>
Vector<T, dim> normalize(Vector<T, dim> const& lhs);

template<class T, std::size_t dim>
Vector<T, dim>& normalize(Vector<T, dim>& lhs);

template<class T, std::size_t dim>
Vector<T, dim> cross(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
Vector<T, dim>& operator++(Vector<T, dim>& lhs);

template<class T, std::size_t dim>
Vector<T, dim> operator++(Vector<T, dim> const& lhs, int);

template<class T, std::size_t dim>
Vector<T, dim>& operator--(Vector<T, dim>& lhs);

template<class T, std::size_t dim>
Vector<T, dim> operator--(Vector<T, dim> const& lhs, int);

template<class T, std::size_t dim>
Vector<T, dim> operator+(Vector<T, dim> const& lhs);

template<class T, std::size_t dim>
Vector<T, dim> operator-(Vector<T, dim> const& lhs);

template<class T, std::size_t dim>
Vector<T, dim> operator+(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> operator+(Vector<T, dim> const& lhs, S const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> operator+(S const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
Vector<T, dim>& operator+=(Vector<T, dim>& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim>& operator+=(Vector<T, dim>& lhs, S const& rhs);

template<class T, std::size_t dim>
Vector<T, dim> operator-(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> operator-(Vector<T, dim> const& lhs, S const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> operator-(S const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
Vector<T, dim>& operator-=(Vector<T, dim>& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim>& operator-=(Vector<T, dim>& lhs, S const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> operator*(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> operator*(Vector<T, dim> const& lhs, S const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> operator*(S const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim>& operator*=(Vector<T, dim>& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim>& operator*=(Vector<T, dim>& lhs, S const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> operator/(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> operator/(Vector<T, dim> const& lhs, S const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim> operator/(S const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim>& operator/=(Vector<T, dim>& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim, class S>
Vector<T, dim>& operator/=(Vector<T, dim>& lhs, S const& rhs);

template<class T, std::size_t dim>
bool operator==(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
bool operator!=(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
bool operator<(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
bool operator<=(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
bool operator>(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

template<class T, std::size_t dim>
bool operator>=(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs);

// ===============================================
// Definitions - default
// ===============================================

/*
 template<class T, unsigned int dim>
 std::ostream& operator<<(std::ostream& os, Vector<T, dim> data)
 {
 os.width(10);
 os << std::fixed << std::setprecision(4) << std::setfill('A');
 os << "(";
 for(unsigned int i = 0; i < dim; ++i)
 {
 os << std::setw(10) << data[i];
 if(i < dim-1) os << ", ";
 else os << ")";
 }

 return os;

 }
 */

template<class T, std::size_t dim>
T & at(Vector<T, dim>& lhs, std::size_t idx)
{
	if (idx >= dim)
		throw std::out_of_range("Index out of range.");
	return lhs.values[idx];
}

template<class T, std::size_t dim>
T const& at(Vector<T, dim> const& lhs, std::size_t idx)
{
	if (idx >= dim)
		throw std::out_of_range("Index out of range.");
	return lhs.values[idx];
}

template<class T, std::size_t dim>
Vector<T, dim>& copy(Vector<T, dim>& lhs, Vector<T, dim> const& rhs)
{
	for (std::size_t i = 0; i < dim; ++i)
	{
		lhs.values[i] = rhs.values[i];
	}
	return lhs;
}

template<class T, std::size_t dim>
Vector<T, dim> uplus(Vector<T, dim> const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = +rhs[i];
	}
	return tmp;
}

template<class T, std::size_t dim>
Vector<T, dim> uminus(Vector<T, dim> const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = -rhs[i];
	}
	return tmp;
}

template<class T, std::size_t dim>
Vector<T, dim> add(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = lhs[i] + rhs[i];
	}

	return tmp;
}

template<class T, std::size_t dim>
Vector<T, dim>& add(Vector<T, dim> & lhs, Vector<T, dim> const& rhs)
{
	for (std::size_t i = 0; i < dim; ++i)
	{
		lhs[i] = lhs[i] + rhs[i];
	}

	return lhs;
}

template<class T, std::size_t dim, class S>
Vector<T, dim> add(Vector<T, dim> const& lhs, S const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = lhs[i] + rhs;
	}

	return tmp;
}

template<class T, std::size_t dim, class S>
Vector<T, dim>& add(Vector<T, dim> & lhs, S const& rhs)
{
	for (std::size_t i = 0; i < dim; ++i)
	{
		lhs[i] = lhs[i] + rhs;
	}

	return lhs;
}

template<class T, std::size_t dim>
Vector<T, dim> sub(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = lhs[i] - rhs[i];
	}

	return tmp;
}

template<class T, std::size_t dim>
Vector<T, dim>& sub(Vector<T, dim> & lhs, Vector<T, dim> const& rhs)
{
	for (std::size_t i = 0; i < dim; ++i)
	{
		lhs[i] = lhs[i] - rhs[i];
	}

	return lhs;
}

template<class T, std::size_t dim, class S>
Vector<T, dim> sub(Vector<T, dim> const& lhs, S const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = lhs[i] - rhs;
	}

	return tmp;
}

template<class T, std::size_t dim, class S>
Vector<T, dim>& sub(Vector<T, dim> & lhs, S const& rhs)
{
	for (std::size_t i = 0; i < dim; ++i)
	{
		lhs[i] = lhs[i] - rhs;
	}

	return lhs;
}

template<class T, std::size_t dim>
Vector<T, dim> mul(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = lhs[i] * rhs[i];
	}

	return tmp;
}

template<class T, std::size_t dim>
Vector<T, dim>& mul(Vector<T, dim> & lhs, Vector<T, dim> const& rhs)
{
	for (std::size_t i = 0; i < dim; ++i)
	{
		lhs[i] = lhs[i] * rhs[i];
	}

	return lhs;
}

template<class T, std::size_t dim, class S>
Vector<T, dim> mul(Vector<T, dim> const& lhs, S const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = lhs[i] * rhs;
	}

	return tmp;
}

template<class T, std::size_t dim, class S>
Vector<T, dim>& mul(Vector<T, dim> & lhs, S const& rhs)
{
	for (std::size_t i = 0; i < dim; ++i)
	{
		lhs[i] = lhs[i] * rhs;
	}

	return lhs;
}

template<class T, std::size_t dim>
Vector<T, dim> div(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = lhs[i] / rhs[i];
	}

	return tmp;
}

template<class T, std::size_t dim>
Vector<T, dim>& div(Vector<T, dim> & lhs, Vector<T, dim> const& rhs)
{
	for (std::size_t i = 0; i < dim; ++i)
	{
		lhs[i] = lhs[i] / rhs[i];
	}

	return lhs;
}

template<class T, std::size_t dim, class S>
Vector<T, dim> div(Vector<T, dim> const& lhs, S const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = lhs[i] / rhs;
	}

	return tmp;
}

template<class T, std::size_t dim, class S>
Vector<T, dim>& div(Vector<T, dim> & lhs, S const& rhs)
{
	for (std::size_t i = 0; i < dim; ++i)
	{
		lhs[i] = lhs[i] / rhs;
	}

	return lhs;
}

template<class T, std::size_t dim, class S>
Vector<T, dim> div(S const& lhs, Vector<T, dim> const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = lhs / rhs[i];
	}

	return tmp;
}

template<class T, std::size_t dim, class S>
Vector<T, dim>& div(S const& lhs, Vector<T, dim> & rhs)
{
	for (std::size_t i = 0; i < dim; ++i)
	{
		rhs[i] = lhs / rhs[i];
	}

	return lhs;
}

template<class T, std::size_t dim>
Vector<T, dim> inc(Vector<T, dim>& lhs)
{
	Vector<T, dim> tmp = lhs;
	for (int i = 0; i < dim; ++i)
	{
		++lhs[i];
	}

	return tmp;
}

template<class T, std::size_t dim>
Vector<T, dim>& inc(Vector<T, dim>& rhs)
{
	for (int i = 0; i < dim; ++i)
	{
		++rhs[i];
	}

	return rhs;
}

template<class T, std::size_t dim>
Vector<T, dim> dec(Vector<T, dim>& lhs)
{
	Vector<T, dim> tmp = lhs;
	for (int i = 0; i < dim; ++i)
	{
		--tmp[i];
	}

	return tmp;
}

template<class T, std::size_t dim>
Vector<T, dim>& dec(Vector<T, dim>& rhs)
{
	for (int i = 0; i < dim; ++i)
	{
		--rhs[i];
	}

	return rhs;
}

template<class T, std::size_t dim>
bool eq(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	bool tmp = true;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp &= (lhs[i] == rhs[i]);
	}

	return tmp;
}

template<class T, std::size_t dim>
bool neq(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	bool tmp = false;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp |= (lhs[i] != rhs[i]);
	}

	return tmp;
}

template<class T, std::size_t dim>
bool lt(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return length(lhs) < length(rhs);
}

template<class T, std::size_t dim>
bool le(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return length(lhs) <= length(rhs);
}

template<class T, std::size_t dim>
bool gt(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return length(lhs) > length(rhs);
}

template<class T, std::size_t dim>
bool ge(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return length(lhs) >= length(rhs);
}

template<class T, std::size_t dim>
T dot(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	T tmp
	{ 0 };
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp += lhs[i] * rhs[i];
	}

	return tmp;
}

template<class T, std::size_t dim>
T length(Vector<T, dim> const& lhs)
{
	return sqrt(lhs.dot(lhs));
}

template<class T, std::size_t dim>
Vector<T, dim> normalize(Vector<T, dim> const& lhs)
{
	Vector<T, dim> tmp;
	T length = vectors::length(lhs);
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = lhs[i] / length;
	}

	return tmp;
}

template<class T, std::size_t dim>
Vector<T, dim>& normalize(Vector<T, dim>& lhs)
{
	T length = vectors::length(lhs);
	for (std::size_t i = 0; i < dim; ++i)
	{
		lhs[i] = lhs[i] / length;
	}

	return lhs;
}

template<class T, std::size_t dim>
Vector<T, dim> cross(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	Vector<T, dim> tmp;
	for (std::size_t i = 0; i < dim; ++i)
	{
		tmp[i] = lhs[(i + 1) % dim] * rhs[(i + 2) % dim]
				- lhs[(i + 2) % dim] * rhs[(i + 4) % dim];
	}

	return tmp;
}

template<class T, std::size_t dim>
Vector<T, dim>& operator++(Vector<T, dim>& lhs)
{
	return vectors::inc(lhs);
}

template<class T, std::size_t dim>
Vector<T, dim> operator++(Vector<T, dim> const& lhs, int)
{
	return vectors::inc(lhs);
}

template<class T, std::size_t dim>
Vector<T, dim>& operator--(Vector<T, dim>& lhs)
{
	return vectors::dec(lhs);
}

template<class T, std::size_t dim>
Vector<T, dim> operator--(Vector<T, dim> const& lhs, int)
{
	return vectors::dec(lhs);
}

template<class T, std::size_t dim>
Vector<T, dim> operator+(Vector<T, dim> const& lhs)
{
	return vectors::uplus(lhs);
}

template<class T, std::size_t dim>
Vector<T, dim> operator-(Vector<T, dim> const& lhs)
{
	return vectors::uminus(lhs);
}

template<class T, std::size_t dim>
Vector<T, dim> operator+(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::add(lhs, rhs);
}

template<class T, std::size_t dim, class S>
Vector<T, dim> operator+(Vector<T, dim> const& lhs, S const& rhs)
{
	return vectors::add(lhs, rhs);
}

template<class T, std::size_t dim, class S>
Vector<T, dim> operator+(S const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::add(rhs, lhs);
}

template<class T, std::size_t dim>
Vector<T, dim>& operator+=(Vector<T, dim>& lhs, Vector<T, dim> const& rhs)
{
	return vectors::add(lhs, rhs);
}

template<class T, std::size_t dim, typename S>
Vector<T, dim>& operator+=(Vector<T, dim>& lhs, S const& rhs)
{
	return vectors::add(lhs, rhs);
}

template<class T, std::size_t dim>
Vector<T, dim> operator-(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::sub(lhs, rhs);
}

template<class T, std::size_t dim, class S>
Vector<T, dim> operator-(Vector<T, dim> const& lhs, S const& rhs)
{
	return vectors::sub(lhs, rhs);
}

template<class T, std::size_t dim, class S>
Vector<T, dim> operator-(S const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::sub(-rhs, -lhs);
}

template<class T, std::size_t dim>
Vector<T, dim>& operator-=(Vector<T, dim>& lhs, Vector<T, dim> const& rhs)
{
	return vectors::sub(lhs, rhs);
}

template<class T, std::size_t dim, class S>
Vector<T, dim>& operator-=(Vector<T, dim>& lhs, S const& rhs)
{
	return vectors::sub(lhs, rhs);
}

template<class T, std::size_t dim>
Vector<T, dim> operator*(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::mul(lhs, rhs);
}

template<class T, std::size_t dim, class S>
Vector<T, dim> operator*(Vector<T, dim> const& lhs, S const& rhs)
{
	return vectors::mul(lhs, rhs);
}

template<class T, std::size_t dim, class S>
Vector<T, dim> operator*(S const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::mul(rhs, lhs);
}

template<class T, std::size_t dim>
Vector<T, dim>& operator*=(Vector<T, dim>& lhs, Vector<T, dim> const& rhs)
{
	return vectors::mul(lhs, rhs);
}

template<class T, std::size_t dim, class S>
Vector<T, dim>& operator*=(Vector<T, dim>& lhs, S const& rhs)
{
	return vectors::mul(lhs, rhs);
}

template<class T, std::size_t dim>
Vector<T, dim> operator/(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::div(lhs, rhs);
}

template<class T, std::size_t dim, class S>
Vector<T, dim> operator/(Vector<T, dim> const& lhs, S const& rhs)
{
	return vectors::div(lhs, rhs);
}

template<class T, std::size_t dim, class S>
Vector<T, dim> operator/(S const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::div(lhs, rhs);
}

template<class T, std::size_t dim>
Vector<T, dim>& operator/=(Vector<T, dim>& lhs, Vector<T, dim> const& rhs)
{
	return vectors::div(lhs, rhs);
}

template<class T, std::size_t dim, class S>
Vector<T, dim>& operator/=(Vector<T, dim>& lhs, S const& rhs)
{
	return vectors::div(lhs, rhs);
}

template<class T, std::size_t dim>
bool operator==(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::eq(lhs, rhs);
}

template<class T, std::size_t dim>
bool operator!=(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::neq(lhs, rhs);
}

template<class T, std::size_t dim>
bool operator<(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::lt(lhs, rhs);
}

template<class T, std::size_t dim>
bool operator<=(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::le(lhs, rhs);
}

template<class T, std::size_t dim>
bool operator>(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::gt(lhs, rhs);
}

template<class T, std::size_t dim>
bool operator>=(Vector<T, dim> const& lhs, Vector<T, dim> const& rhs)
{
	return vectors::ge(lhs, rhs);
}

template<class Container, class T, std::size_t dim, typename = void>
struct x_component
{
};

template<class Container, class T, std::size_t dim, typename = void>
struct y_component
{
};

template<class Container, class T, std::size_t dim, typename = void>
struct z_component
{
};

template<class Container, class T, std::size_t dim, typename = void>
struct w_component
{
};

template<class Container, class T, std::size_t dim>
struct x_component<Container, T, dim, std::enable_if_t<(dim > 0)>>
{
	T& x;
	T& r;

	x_component(Container& self) :
			x(self.values[0]), r(self.values[0])
	{

	}

	x_component& operator=(x_component const&)
	{
		return *this;
	}
};

template<class Container, class T, std::size_t dim>
struct y_component<Container, T, dim, std::enable_if_t<(dim > 1)>>
{
	T& y;
	T& g;

	y_component(Container& self) :
			y(self.values[1]), g(self.values[1])
	{

	}

	y_component& operator=(y_component const&)
	{
		return *this;
	}
};

template<class Container, class T, std::size_t dim>
struct z_component<Container, T, dim, std::enable_if_t<(dim > 2)>>
{
	T& z;
	T& b;

	z_component(Container& self) :
			z(self.values[2]), b(self.values[1])
	{

	}

	z_component& operator=(z_component const&)
	{
		return *this;
	}
};

template<class Container, class T, std::size_t dim>
struct w_component<Container, T, dim, std::enable_if_t<(dim > 3)>>
{
	T& w;
	T& a;

	w_component(Container& self) :
			w(self.values[3]), a(self.values[3])
	{

	}

	w_component& operator=(w_component const&)
	{
		return *this;
	}
};

template<class T, std::size_t dim>
class Vector: public x_component<Vector<T, dim>, T, dim>,
		public y_component<Vector<T, dim>, T, dim>,
		public z_component<Vector<T, dim>, T, dim>,
		public w_component<Vector<T, dim>, T, dim>
{
public:
	T values[dim] __attribute((aligned(16)));
public:
	template<class R, unsigned int fdim>
	friend R& at(Vector<R, fdim>& lhs, unsigned int idx);

	template<class R, unsigned int fdim>
	friend R const& at(Vector<R, fdim> const& lhs, unsigned int idx);

	template<class R, unsigned int fdim>
	friend Vector<R, fdim>& copy(Vector<R, fdim>& lhs,
			Vector<R, fdim> const& rhs);

public:
	Vector() :
			x_component<Vector<T, dim>, T, dim>(*this), y_component<
					Vector<T, dim>, T, dim>(*this), z_component<Vector<T, dim>,
					T, dim>(*this), w_component<Vector<T, dim>, T, dim>(*this), values
			{ }
	{
	}

	Vector(std::initializer_list<T> init) :
			x_component<Vector<T, dim>, T, dim>(*this), y_component<
					Vector<T, dim>, T, dim>(*this), z_component<Vector<T, dim>,
					T, dim>(*this), w_component<Vector<T, dim>, T, dim>(*this)
	{
		unsigned int i = 0;
		for (T val : init)
		{
			if (i >= dim)
				throw std::invalid_argument("Too many values.");
			values[i] = val;
			++i;
		}

		if (i < (dim - 1))
		{
			for (; i < dim; ++i)
			{
				values[i] =
				{};
			}
		}
	}

	Vector(Vector<T, dim> const& rhs) :
	x_component<Vector<T, dim>, T, dim>(*this),
	y_component<Vector<T, dim>, T, dim>(*this),
	z_component<Vector<T, dim>, T, dim>(*this),
	w_component<Vector<T, dim>, T, dim>(*this)
	{
		vectors::copy(*this, rhs);
	}

	~Vector()
	{

	}

	T & operator[](unsigned int idx)
	{
		return at(*this, idx);
	}

	T const& operator[](unsigned int idx) const
	{
		return at(*this, idx);
	}

	template<class R, unsigned int fdim>
	friend Vector<R, fdim> operator+(Vector<R, fdim> const& lhs);
	template<class R, unsigned int fdim>
	friend Vector<R, fdim> operator-(Vector<R, fdim> const& lhs);
	template<class R, unsigned int fdim>
	friend Vector<R, fdim>& operator++(Vector<R, fdim>& lhs);
	template<class R, unsigned int fdim>
	friend Vector<R, fdim>& operator--(Vector<R, fdim>& lhs);
	template<class R, unsigned int fdim>
	friend Vector<R, fdim> operator++(Vector<R, fdim>& lhs, int);
	template<class R, unsigned int fdim>
	friend Vector<R, fdim> operator--(Vector<R, fdim>& lhs, int);

	template<class R, unsigned int fdim>
	friend Vector<R, fdim> operator+(Vector<R, fdim> const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator+(Vector<R, fdim> const& lhs, S const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator+(S const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim>
	friend Vector<R, fdim>& operator+=(Vector<R, fdim>& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim>& operator+=(Vector<R, fdim>& lhs, S const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator+=(S const& lhs, Vector<R, fdim> const& rhs);

	template<class R, unsigned int fdim>
	friend Vector<R, fdim> operator-(Vector<R, fdim> const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator-(Vector<R, fdim> const& lhs, S const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator-(S const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim>
	friend Vector<R, fdim>& operator-=(Vector<R, fdim>& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim>& operator-=(Vector<R, fdim>& lhs, S const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator-=(S const& lhs, Vector<R, fdim> const& rhs);

	template<class R, unsigned int fdim>
	friend Vector<R, fdim> operator*(Vector<R, fdim> const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator*(Vector<R, fdim> const& lhs, S const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator*(S const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim>
	friend Vector<R, fdim>& operator*=(Vector<R, fdim>& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim>& operator*=(Vector<R, fdim>& lhs, S const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator*=(S const& lhs, Vector<R, fdim> const& rhs);

	template<class R, unsigned int fdim>
	friend Vector<R, fdim> operator/(Vector<R, fdim> const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator/(Vector<R, fdim> const& lhs, S const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator/(S const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim>
	friend Vector<R, fdim>& operator/=(Vector<R, fdim>& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim>& operator/=(Vector<R, fdim>& lhs, S const& rhs);
	template<class R, unsigned int fdim, class S>
	friend Vector<R, fdim> operator/=(S const& lhs, Vector<R, fdim> const& rhs);

	template<class R, unsigned int fdim>
	friend bool operator==(R const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim>
	friend bool operator!=(R const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim>
	friend bool operator<(R const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim>
	friend bool operator<=(R const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim>
	friend bool operator>(R const& lhs, Vector<R, fdim> const& rhs);
	template<class R, unsigned int fdim>
	friend bool operator>=(R const& lhs, Vector<R, fdim> const& rhs);

public:
	T dot(Vector<T, dim> const& rhs) const
	{
		return vectors::dot(*this, rhs);
	}

	T length() const
	{
		return vectors::length(*this);
	}

	Vector<T, dim>& normalize()
	{
		return vectors::normalize(*this);
	}

	Vector<T, dim> normalize() const
	{
		return vectors::normalize(*this);
	}

	Vector<T, dim> cross(Vector<T, dim> const& rhs) const
	{
		return vectors::cross(*this, rhs);
	}
};

template<class T, std::size_t dim>
class VectorRef
{
public:
	Vector<T, dim>& base;

	VectorRef(Vector<T, dim>& vec) :
			base(vec)
	{

	}
};

}
}





#endif /* INCLUDE_VECTOR_BASE_H_ */
