#ifndef VECTOR_HPP_
#define VECTOR_HPP_

#include "vector_base.h"

#ifdef __AVX__
#include "vector_avx.h"
#elif defined(__SSE2__)
#include "vector_sse2.h"
#endif

namespace math
{
namespace vectors
{

// ===============================================
// Convenience Typedefs
// ===============================================

typedef Vector<double, 2> vec2;
typedef Vector<float, 2> vec2f;

typedef Vector<double, 3> vec3;
typedef Vector<float, 3> vec3f;

typedef Vector<double, 4> vec4;
typedef Vector<float, 4> vec4f;
}
}



#endif /* VECTOR_HPP_ */
